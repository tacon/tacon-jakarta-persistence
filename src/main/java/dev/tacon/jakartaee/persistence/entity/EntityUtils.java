package dev.tacon.jakartaee.persistence.entity;

import java.io.Serializable;

/**
 * A utility class providing helper methods for working with JPA entities.
 * This class is intended to be used within a Jakarta EE environment.
 */
public final class EntityUtils {

	/**
	 * Retrieves the identifier of the given {@link AbstractJpaEntity}, or returns {@code null} if the entity is null.
	 *
	 * @param <T> the type of the identifier, which must be serializable.
	 * @param entity the entity whose identifier is to be retrieved.
	 * @return the identifier of the entity, or {@code null} if the entity itself is {@code null}.
	 */
	public static <T extends Serializable> T id(final AbstractJpaEntity<T> entity) {
		return entity != null ? entity.getId() : null;
	}

	private EntityUtils() {
		throw new AssertionError();
	}
}